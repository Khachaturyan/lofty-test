<?php

namespace App\Traits;


trait ResponseTrait
{
    /**
     * @param string $message
     * @param array $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSuccessResponse(string $message, array $data = [], int $status = 200)
    {

        $return = [
            'status' => 1,
            'data' => $data
        ];

        if (is_string($message))
            $return['message'] = $message;
        if (is_array($message))
            $return = array_merge($return, $message);

        return response()->json($return, $status);
    }

    /**
     * @param $message
     * @param array $errors
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFailResponse(string $message, array $errors = [], int $status = 400)
    {
        return response()->json([
            'status' => 0,
            'message' => $message,
            'errors' => $errors,
            'data' => []
        ], $status);
    }
}
