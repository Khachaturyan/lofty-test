<?php

namespace App\Services;

use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Validation
{
    use ResponseTrait;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|regex:/^[0-9a-fA-F]{3}-[0-9a-fA-F]{3}-[0-9a-fA-F]{3}-[0-9a-fA-F]{3}$/',
            'ct' => 'required|numeric|between:0,9999999.99|gt:0'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
    }

    public function validateId(string $id)
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'exists:counts,id'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
    }
}
