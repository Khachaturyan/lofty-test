<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Count;
use App\Services\Validation;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $validation = new Validation();
        if ($validation->validateRequest($request)) {
            return $validation->validateRequest($request);
        }

        $count = Count::find($request->id);

        if ($count) {
            $count->update([
                'ct' => $request->ct
            ]);
        } else {
            $count = Count::create([
                'id' => $request->id,
                'ct' => $request->ct
            ]);
        }

        return $this->getSuccessResponse('ok', $count->toArray());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->getSuccessResponse('ok', Count::all()->toArray());
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $id)
    {
        $validation = new Validation();
        if ($validation->validateId($id)) {
            return $validation->validateId($id);
        }
        return $this->getSuccessResponse('ok', Count::find($id)->toArray());
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(string $id)
    {
        $validation = new Validation();
        if ($validation->validateId($id)) {
            return $validation->validateId($id);
        }
        Count::find($id)->delete();

        return $this->getSuccessResponse('ok');

    }
}
