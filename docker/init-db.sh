#!/bin/bash
set -e

# Создаем пользователя
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER lofty WITH PASSWORD 'lofty';
EOSQL

# Создаем базу данных
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE DATABASE lofty;
    GRANT ALL PRIVILEGES ON DATABASE lofty TO lofty;
EOSQL
