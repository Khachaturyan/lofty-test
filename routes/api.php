<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('count','App\Http\Controllers\Api\IndexController@store');
Route::get('count','App\Http\Controllers\Api\IndexController@index');
Route::get('count/{id}','App\Http\Controllers\Api\IndexController@show');
Route::delete('count/{id}','App\Http\Controllers\Api\IndexController@delete');
