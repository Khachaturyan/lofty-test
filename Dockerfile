FROM php:8.1-apache

RUN apt-get update \
    && apt-get install -y \
        git \
        libpq-dev \
        unzip \
        && docker-php-ext-install pdo pdo_pgsql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /var/www/html

COPY /docker/apache2.conf /etc/apache2/apache2.conf

COPY docker/php.ini /usr/local/etc/php/

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

COPY docker/init-db.sh /docker-entrypoint-initdb.d/init-db.sh

WORKDIR /var/www/html

RUN composer update

RUN php artisan migrate --force

EXPOSE 80

CMD ["apache2-foreground"]
